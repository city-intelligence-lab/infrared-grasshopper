# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System
import rhinoscriptsyntax as rs
import Rhino
import json
import scriptcontext as sc
import ghpythonlib.components as ghcomp
import ghpythonlib.parallel as pr
from clr import GetClrType as typeof

import InFraReDUtils as utils


class SyncSnapshot(component):

    def __init__(self):
        super(SyncSnapshot, self).__init__()

        # Defaults (None indicates required input params)
        self.N = None
        self.D = ''
        self.R = 'origin'
        self.B = {}
        self.S = {}
        self.P = {}
        self.Sb = None

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Sync Snapshot',
            'IR Sync',
            'Use this component to update existing, or create a new snapshot and sync it with the InFraReD server.',
            'InFraReD',
            '4 | Snapshot'
        )
        return instance
    
    def get_ComponentGuid(self):
        return System.Guid('829269aa-a415-4701-89c1-b62e0110e5a8')
    
    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional
    
    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Name', 'N', 'The name of your snapshot.\nIf the name already exists, the corresponding data will be overwritten.\n If no snapshot with the name exists, a new one will be created.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Description', 'D', 'A short description about your snapshot', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Root Snapshot', 'R', "The root snapshot of the snapshot you'd like to create.\nThis can help you organise (nested) design variants. The information will be used to create a tree view of design variants. Defaults to the base snapshot that is automatically created when a project is created 'origin'.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Buildings', 'B', 'Supply InFraReD Buildings here (from the "InFraReD Create Buildings" component).', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Streets', 'S', 'Supply InFraReD Streets here (from the "InFraReD Create Streets" component).', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'POIs', 'P', 'Supply InFraReD POIs here (from "InFraReD Create POIs" component).', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_Rectangle()
        self.SetUpParam(p, 'Site Boundary', 'Sb', 'A rectangle that represents the bounds of your project area.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Snapshot Metadata', 'm', 'Metadata of the synced snapshot in the InFraReD Schema format.')
        self.Params.Output.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Buildings InFraReD Schema', 'B', 'InFraReD Schema for Buildings as GH geometry.')
        self.Params.Output.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Streets InFraReD Schema', 'S', 'InFraReD Schema for Streets as GH geometry.')
        self.Params.Output.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'POIs InFraReD Schema', 'P', 'InFraReD Schema for POIs as GH geometry.')
        self.Params.Output.Add(p)
    
    def SolveInstance(self, DA):
        N = self.marshal.GetInput(DA, 0)
        D = self.marshal.GetInput(DA, 1)
        R = self.marshal.GetInput(DA, 2)
        B = self.marshal.GetInput(DA, 3)
        S = self.marshal.GetInput(DA, 4)
        P = self.marshal.GetInput(DA, 5)
        Sb = self.marshal.GetInput(DA, 6)
        results = self.RunScript(N, D, R, B, S, P, Sb)

        if results is not None:
            self.marshal.SetOutput(results[0], DA, 0, True)
            self.marshal.SetOutput(results[1], DA, 1, True)
            self.marshal.SetOutput(results[2], DA, 2, True)
            self.marshal.SetOutput(results[3], DA, 3, True)
        
    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAAB+klEQVRIiWNkYGBgYExIdWBgYABhaoMN/xfMvsDImJAawMDAsJ4GFsBAIBMDA4MBDS0AgXgmGlsAAgIsyDwHDXUGew01OP/Bm7cMC48cwymuICLMEG9jhWJi44bNGLag+ARkkIO6OtlOrvf3xSrOgi5w4OZNDNccuHETjNEByEfIanFZQo84QfUJyGWUgIM3bhG2BBSZlADHzh6sulGCC5RaQJhcAEqFBC0BJUf0JEkK2FdeTDi4iAUGcrIM2S5ODF9//gTr4GZnZ5i6Zx9O3WRZ0hwUwDD76HGGL1BLQKDc25MySwS4uBjy3ZzhfG99XTDGBuoDIHll4q69DB++fQOzQaVwA0gOnyUgjeefPkdxOT7Aw87OYCgtCcuoB1B8AktZ2PILugX6MtIMQUaQAnzX1esMF588ZSh1d2FYdPwkw93Xb1DUkpW6QBb0hgaBDQPhRj9vBmtlJbC4BB8fhnqyIl5fVoZh3fkLYFczgIOHjUGcn48hYNosrOrJLruURUXhbHEsrkcGGD4BlaSw0hRUFmErKtadu8AQZKjPMDMmEmoJL8P0A4eJswSUGrBVOugAlAhi5i4ExwMoI+66dh1vyiMqTkDpHWQgukEvPn0C08qiIijioCT84esXFEs+ELIElLFwFX64AFIltxHW7uqnUavlAQMDQyEA4pmrYtLvA14AAAAASUVORK5CYII='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def snapshotThumb(self, crv):
        # get center pt and dimensions
        bbox = ghcomp.BoundingBox(crv)
        bbox = bbox[0]
        centerPT = bbox.Center

        H = bbox.BoundingBox.Max.Y- bbox.BoundingBox.Min.Y 
        W = bbox.BoundingBox.Max.X - bbox.BoundingBox.Min.X

        position = (
            centerPT.X,
            centerPT.Y - H * 1.5,
            1000
        )

        maximizedID = None
        allVP = Rhino.RhinoDoc.ActiveDoc.Views.GetViewList(True, False)
        for i, vp in enumerate(allVP):
            if vp.Maximized == True:
                maximizedID = i

        # Rhino.RhinoDoc.ActiveDoc.Views.Add(
        cam_view = Rhino.RhinoDoc.ActiveDoc.Views.Add(
            'SnapThumbnail',
            Rhino.Display.DefinedViewportProjection.Perspective,
            System.Drawing.Rectangle(4, 4, 4, 4),
            True
        )

        cam_view_ID = cam_view.ActiveViewportID
        rs.ViewCameraTarget(cam_view_ID, position, centerPT)
        rs.ViewCamera(cam_view_ID, position)
                                
        imageDim = System.Drawing.Size(400, 400)
        
        idd = Rhino.Display.DisplayModeDescription.Id
        dm = Rhino.Display.DisplayModeDescription.GetDisplayModes()

        # get prefered render mode
        for obj in dm:
            if obj.LocalName == 'Rendered':
                dm = obj
                                
        imageCap = Rhino.Display.RhinoView.CaptureToBitmap(cam_view, imageDim, dm)
        cam_view.Close()
        
        if maximizedID != None:
            allVP[maximizedID].Maximized = True
        
        converter = System.Drawing.ImageConverter()
        bytes_ = converter.ConvertTo(imageCap, typeof(System.Array[System.Byte]))
        imageB64 = System.Convert.ToBase64String(
            bytes_,
            System.Base64FormattingOptions.None
        )
                                
        return imageB64
                            
    def extrudeBuilding(self, obj):
        face = ghcomp.BoundarySurfaces(obj[0])
        extrusion = ghcomp.Extrude(face, rs.CreateVector(0, 0, obj[1]))
        return extrusion

    # Add options via drop-down:
    # -> capture view
    # -> output geometry
    # ... more elegant way to caputre viewport (get camera, move it take scree, move back?)
    # def RunScript(self, name, description, rootSnap, buildings, streets, POI, siteBoundary):
    def RunScript(self, N, D, R, B, S, P, Sb):
        results = None

        # Validate authentication
        auth_token = sc.sticky.get('InFraReDAuthToken')
        if not auth_token:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                'Invalid Credentials.\nPlease authenticate with the InFraReD server using the "InFraReD Authenticate" component.'
            )
            return

        # Grab stickies
        client_uuid = sc.sticky.get('InFraReDClientUuid')

        # Input parameter checking and defaults
        if N is not None:
            self.N = N
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter N failed to collect data.'
            )
            return
        if R is not None:
            self.R = R
        if B is not None:
            self.B = B
        if S is not None:
            self.S = S
        if P is not None:
            self.P = P
        if self.B or self.S or self.P:
            pass
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                "C'mon dude, ya gotta gimme summit to work wiff.\nWe're talking buildings, streets, POIs, you name it!"
            )
            return
        # TODO: Check geo inputs for correct data type
        if Sb is not None:
            self.Sb = Sb
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter Sb failed to collect data.'
            )
            return
        
        # Get project uuid
        project_uuid = sc.sticky.get('InFraReDProjectUuid')
        if not project_uuid:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                'No Project Found.\nPlease create a project using the "InFraReD Create Project" component or load one using the "InFraReD Load Project" component.'
            )
            return
            
        # Get snapshot list from server 
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getSnapshotsByProjectUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            project_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['getSnapshotsByProjectUuid']
        if return_data['success'] == True:
            snapshots = return_data['infraredSchema']['clients'][client_uuid]['projects'][project_uuid]['snapshots']

        # Make sure root snap is in snapshost
        rootSnapUUID = utils.match_snaps(snapshots, self.R)
        if not rootSnapUUID:
            self.R = 'origin'
            rootSnapUUID = utils.match_snaps(snapshots, self.R)

        self.Message = 'active: [' + self.N + ']\nroot: [' + self.R + ']'

        if self.D == '':
            self.D = 'This snapshot has '
            try:
                self.D += str(len(self.B['geometry']))
            except:
                self.D += str(0)
            self.D += ' buildings, '
            try:
                self.D += str(len(self.S['geometry'])) 
            except:
                self.D += str(0)
            self.D += ' street segments, and '
            try:
                self.D += str(len(self.P['geometry']))
            except:
                self.D += 'no'
            self.D += ' points of interest.'
        
        # IRjson head 
        IRjson = {
            'snapshotName': self.N,
            'snapshotDescription': self.D,
            'snapshotThumbnail': self.snapshotThumb(self.Sb),
            'previousSnapshot': rootSnapUUID,
            'buildings': {},
            'streets': {},
            'POI': {}
        }
                                
        GHgeo = {
            'buildings': [],
            'streets': [],
            'POI': []
        }

        def mergeJson(targetDict, sourceList, objKey):
            for i in range(len(sourceList)):
                targetDict[objKey][objKey + str(i)] = sourceList[i]
                                
        # merge geoJsons to IRjson
        try:
            mergeJson(IRjson, self.B['json'], 'buildings')
            #GHgeo["buildings"] = buildings["geometry"]
        except:
            pass
                                
        try:
            mergeJson(IRjson, self.S['json'], 'streets')
            #GHgeo["streets"] = streets["geometry"]
        except:
            pass
                                
        try:
            mergeJson(IRjson, self.P['json'], 'POI')
            #GHgeo["POI"] = POI["geometry"]
        except:
            pass
        
        #wrap 
        m = [IRjson]
        
        # generate or pass through gh geometry
        try:
            buildingsIR = pr.run(self.extrudeBuilding, self.B['geometry'])
        except:
            buildingsIR = 'There are no buildings in this project'
        try:
            streetsIR = self.S['geometry']
        except:
            streetsIR = 'There are no no streets in this project'
        try:
            poiIR = self.P['geometry']
        except:
            poiIR = 'There are no POI in this project'
        
        # new or old snapshot?
        curSnapUuid = utils.match_snaps(snapshots, self.N)

        # if old snapshot, delete it
        if curSnapUuid:
            try:
                gql = System.Text.RegularExpressions.Regex.Replace("""
                    {{ \" query \":
                        \" mutation {{
                            deleteSnapshot (
                                uuid: \\\" {} \\\",
                                projectUuid: \\\" {} \\\"
                            ) {{
                                success
                            }}
                        }} \"
                    }}
                    """,
                    "\s+",
                    ""
                ).format(
                    curSnapUuid,
                    project_uuid
                )
                _, auth_token = utils.send_gql_req_to_api(gql, auth_token)
            except:
                pass

        # GQL to create new snapshot
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" mutation {{
                    syncGrasshopperSnapshot (
                        infraredSchema: \\\" {} \\\",
                        projectUuid: \\\" {} \\\"
                    ) {{
                        success
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            json.dumps(IRjson).replace('"', '\\\\\\\"'),
            project_uuid
        )
        _, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # 

        # Update Auth token
        sc.sticky['InFraReDAuthToken'] = auth_token
        sc.sticky['InFraReDProjectUuid'] = project_uuid
        sc.sticky['InFraReDSnapshotList'] = snapshots 
        # NOTE: the snapshots sticky currently doesn't include the new snapshot...
        # but it doesn't make too much sense to query for a list of snapshots here
        # again when any other component that would need it would query for it itself...

        results = [m, buildingsIR, streetsIR, poiIR]
        return results


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):
    def get_AssemblyName(self):
        return 'SyncSnapshot'
    
    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'
    
    def get_Id(self):
        return System.Guid('6fcb7faf-bb30-4c3c-a4c2-a6f7e696ead5')