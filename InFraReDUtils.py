# -*- coding: utf-8 -*-
import System
import Rhino
import string

def get_icon(analysis_type):
    AnalysisIcons = {
        "solar-radiation": "iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABQklEQVRIie1V3U3DMBD+jBggbNARGKGZADbg+uBnyAYwQcuzHzgmACZoR+gGMEI2OHTSRXItu7EReesnRYp99vfdT+6CCxaDI79y5J9a+a8az68AbJcW+ROyIo782pEXR54y5jFz/s2R/26N5GiPXr6fNoXDQTjcJAKaPnXmUBJxRQP5TkUAfAH4BKAFv7O6qAPvwoE1Co1OOAzNIonYHsBtxszCYTPHUVP4bUFAQYW61YtYFHMkD3Mi1wmppmVtS63D6xxBdFei5SAcdlkRNQLo7P2nVsDQR+/HEwcqPIyjy+FFODyf46gp/JBrwMjjXcFWJ2LDsLNUpM3Gtk9xwzbBRoXEU1e/Nh05iSMfZ0ZQORIjImu2OB3a7Y/J8c00gppE7FKf6WZN3UlqhMNoaevxH5imcytV6/9ktCa9YCEA+AVvy2CGNS6RIwAAAABJRU5ErkJggg==",
        "sunlight-hours": "iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACLklEQVRIie1UvWsUQRT/jQbc2OxySk6JmE2OkJBqZUWEgJwWwjVirAQR1mILCyHpUumlsswfsIXbJdokXcocgoIi60ZQTlQYBfETvSObmFQrbzNZJ3uzZ9aPLj+Y4s28eb/3jT38NzDHNZnjLhS1v6+gvgng4j8jYY5rFTVWiIQ5bhXAU+a4TuapBWAxo2swxyXd73kk+5W3YcCZZVNq6syyVxAGTXH/EWFwdweJZc8BIKeuIwxCpdNdoiQvJ8m0iOCWVA+SfQAzABKd2PcWc+10I8Gv2iwDMFQxx7534nc21OmSSbbSMZq9L+kGSrp+ZHV4xGKW/Qhh0Mq1kfHaFG1K4CItOwo6fHwAV2oXiCCRf2xuoPHkMZYe3J8Sqd2OMCXNdtcdkRo6swCsLMGNy1dTgvefP6H3gIba+BlcOnd+Vvo7Kf/LkkwAOEsn9r0JybMEFIGMZ69eplL15CkMHO3n4m9d1uuRBRFiQ5aZ45Jc7e8rpxHkYWyoYvLb9Ub2eTdr5RrV5qCmdTx8a7d38b37WjHEMqRBG/zw9UuHhxSdnLJ2FCk7LG+tGKKANHwtSlu0vj5DXSSD6iBH9XAl8FX2lHPCLPs0gGlKVex788llGPCmbpgl3bCO9ZVT3fKhwwmBt3APq2trNYTBRoc9FUkexOJcHhuq8FGzYvZqGl6/e4smf+O3o2hKng0ZPUVItvH85vTgi63BNWLfUy7FvyHhYilSe/M/cXAP3QHgJ3ytutg1+4kgAAAAAElFTkSuQmCC",
        "wind-speed": "iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABk0lEQVRIie2Uz0oCURTGPyOQVjNYaCXiLGpnaSC6CGIyENpFvcC4cB0+QfQEPcAsml6gPyuhRQ5BCyNIw03MxjGNlBDdpK4mznBHSkc0kmgxH1zOZc6953fOuYeBo38n16iEXFKaB3AiRmOS+nBfBlAAUGS2YChyedJiZgcCRwBYl3OBxSVhP5FErVEXtIou+L2+vYOdJKr1OmqhMPKlosqgOgOrYyEAHgFkABwCOF5e8J7Sx5VAEFpFJ5i5p0XSXnSx2W6J8VAY8bV1VBNJnN9cU5IZQ5EvraAzNuAjAClDkRUPxw05CWRpnvmrjTcTLEZj8HC8ACDy9Y4dhDJp2TZ3hDq9Xt8x53aPbReJ+nrhktKpZrs95PR7ff39R7dr2tVAEHSWKmKVFsZBrgCcEej1vUEV8VaLPByPTq9rBnrSnsmSv5AvFWnR5JXtHv/bCLuktAFgmw6yEc7tbm5Fsne3VnZlNsYqCzjRGNtVYspQZMpyIwuIo0ZzUtk9/CDsVwBMApmGHIgD+TvIj36MjqYvAJ+toJnTYB0a6wAAAABJRU5ErkJggg==",
        "wind-comfort": "iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABp0lEQVRIid1Vy1HDMBB9YiggdAAVgCsgVECoAOWgM0kHoYIkZx8iOggVOFQAVBB3QDoQszNPzI4sx8aTU3ZGE3u13rf79hOcvxjrxnJOkajJKq37BHDN1zr4sjDWyfsSwIj6dfDldhAIo68AFAIAYA9gSoADgFcA9wBmAJ76AF2miuDLXQQn4IhZybkKvhSgrbFO9M/y3BvEWBdabDzPjgBaRvx2AeBWsgy+/Gqwk9DUV+5I35T2G/5K/W6O0VX9A+TAqL2xbtZlrEEeOmwlylorWBehZ6eaoiGarraaaJlK9AqgInVoowptc5I1tE5qMBFHGYAoRa7wFx2OfxTn32oQNwRIHb7k/BwFYesujXUVHXvqx6xDndhbyZjboR9I8OWcbfrBesx5FQuuna0ILJnvjXV/VDYmPgPkj1zrmrxLEMzCstv6gbRIwSz0jDxyKwiFC/1ZV02ywg6KVMWIZ21bYxCIihwcwBWfB3VXVox1E3bYgVt4TbvJSUA4iHEhyv6K66bmfTqggzKxsYWDL1dKHwezATKku4QeAUn/rN6oTwf0XATAL19XmK25ZlVHAAAAAElFTkSuQmCC",
    }
    img = System.Drawing.Bitmap(
        System.IO.MemoryStream(
            System.Convert.FromBase64String(
                AnalysisIcons[analysis_type]
            )
        )
    )
    return img

def split_string(s):
    return s.translate({ord(c): None for c in string.whitespace})

def send_gql_req_to_api(gql, auth_token):
    
    # Set up request
    request = System.Net.HttpWebRequest.Create('https://prototype.infrared.city/api')
    request.Method = 'POST'
    request.ContentType = 'application/json'
    
    # Set up auth cookie
    request.CookieContainer = System.Net.CookieContainer()
    auth_cookie = System.Net.Cookie('InFraReD', auth_token, '/', 'prototype.infrared.city')
    request.CookieContainer.Add(auth_cookie)
    
    # Add GQL data to request
    byte_data = System.Text.Encoding.UTF8.GetBytes(gql)
    request.ContentLength = byte_data.Length
    request_stream = request.GetRequestStream()
    request_stream.Write(byte_data, 0, byte_data.Length)
    request_stream.Close()
    
    # Send request
    try:
        response = request.GetResponse()
    except Exception, ex:
        print(ex)
        raise Exception(" Server Error.\nThere is an issue with the server. Please double check your inputs and workflow or try again later.")
        
    # Extract cookies
    for cookie in response.Cookies.GetEnumerator():
        if cookie.Name == 'InFraReD':
            auth_token = cookie.Value
            break
    
    # Read output
    if response.StatusDescription == 'OK':
        stream = response.GetResponseStream()
        reader = System.IO.StreamReader(stream)
        out = reader.ReadToEnd()
        response.Close()
    
    return out, auth_token

class Analysis:
    def __init__(self, analysis):
        self.analysis = analysis
        self.type = 'Analysis'

def check_attr(atribute, reference, default):
    # Note: could get those from auth component via stickies?
    try:
        reference[atribute]
        value = atribute
    except KeyError:
        value = default
    return value

def str_rounder(x, dp):
    assert dp > 0, 'dp should be bigger than 0'
    str_rep = str(round(x, dp)).split('.')
    if len(str_rep) == 1:
        return str_rep[0] + '.' + '0'*dp
    else:
        max_dec = len(str_rep[1][:dp])
        return str_rep[0] + '.' + str_rep[1][:dp] + '0'*(dp-max_dec)

def match_snaps(snap_list, snap_name):
    snapUUID = None
    for uuid in snap_list.keys():
        curName= snap_list[uuid]['snapshotName']
        if curName == snap_name:
            snapUUID = uuid
    return snapUUID