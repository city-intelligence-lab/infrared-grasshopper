# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System

import InFraReDUtils as utils

class SunlightHoursAnalysisContainer(utils.Analysis):
    def __repr__(self):
        return '<InFraReD.SunlightHoursAnalysis instance at {:#018x}>'.format(
            id(self)
        )

class SunlightHoursAnalysis(component):

    def __init__(self):
        super(SunlightHoursAnalysis, self).__init__()

        # Defaults (None indicates required input params)
        self.v = {}
        self.f = {}

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Sunlight Hours Analysis',
            'IR Sunlight Hours',
            "One of InFraReD's analysis modules. Merge multiple of these analysis components to define all the analyses ran on your project.\n\nThe sunlight hours analysis module leverages a deep learning model and was trained on data from Vienna, Austria.",
            'InFraReD',
            '1 | Analysis'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid("1cd37929-793e-4060-a979-585484c43877")

    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Visualisation", "v", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Filters", "f", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Analysis Container', 'A', 'A container for this analysis and the arguments.')
        self.Params.Output.Add(p)

    def SolveInstance(self, DA):
        v = self.marshal.GetInput(DA, 0)
        f = self.marshal.GetInput(DA, 1)

        result = self.RunScript(v, f)
        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)

    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACLklEQVRIie1UvWsUQRT/jQbc2OxySk6JmE2OkJBqZUWEgJwWwjVirAQR1mILCyHpUumlsswfsIXbJdokXcocgoIi60ZQTlQYBfETvSObmFQrbzNZJ3uzZ9aPLj+Y4s28eb/3jT38NzDHNZnjLhS1v6+gvgng4j8jYY5rFTVWiIQ5bhXAU+a4TuapBWAxo2swxyXd73kk+5W3YcCZZVNq6syyVxAGTXH/EWFwdweJZc8BIKeuIwxCpdNdoiQvJ8m0iOCWVA+SfQAzABKd2PcWc+10I8Gv2iwDMFQxx7534nc21OmSSbbSMZq9L+kGSrp+ZHV4xGKW/Qhh0Mq1kfHaFG1K4CItOwo6fHwAV2oXiCCRf2xuoPHkMZYe3J8Sqd2OMCXNdtcdkRo6swCsLMGNy1dTgvefP6H3gIba+BlcOnd+Vvo7Kf/LkkwAOEsn9r0JybMEFIGMZ69eplL15CkMHO3n4m9d1uuRBRFiQ5aZ45Jc7e8rpxHkYWyoYvLb9Ub2eTdr5RrV5qCmdTx8a7d38b37WjHEMqRBG/zw9UuHhxSdnLJ2FCk7LG+tGKKANHwtSlu0vj5DXSSD6iBH9XAl8FX2lHPCLPs0gGlKVex788llGPCmbpgl3bCO9ZVT3fKhwwmBt3APq2trNYTBRoc9FUkexOJcHhuq8FGzYvZqGl6/e4smf+O3o2hKng0ZPUVItvH85vTgi63BNWLfUy7FvyHhYilSe/M/cXAP3QHgJ3ytutg1+4kgAAAAAElFTkSuQmCC'
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, v, f):
        result = None
        
        # Input parameter checking and defaults
        if v is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.v = v
            pass
        if f is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.f = f
            pass
            
        # Compose session settings analysis
        result = SunlightHoursAnalysisContainer(
            {
                "name": "Sunlight Hours",
                "parameters": []
            }
        )
        
        return result


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'SunlightHoursAnalysis'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('c05b3d87-ce1a-4fcc-8056-056d50bbb624')