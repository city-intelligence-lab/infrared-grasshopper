# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System
import Rhino
import rhinoscriptsyntax as rs
import Grasshopper.Kernel.Data.GH_Path as ghpath
import Grasshopper.DataTree as tree

import InFraReDUtils as utils


class ReadAnalyses(component):

    def __init__(self):
        super(ReadAnalyses, self).__init__()
        
        # Defaults (None indicates required input params)
        self.A = None
        self.K = []
    
    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Read Analyses',
            'IR Read',
            'Use this component to read the analyses results returned from the "InFraReD Run Analyses".',
            'InFraReD',
            '5 | Run'
        )
        return instance
    
    def get_ComponentGuid(self):
        return System.Guid('329326cb-de65-43d7-928f-529e87be1190')
    
    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Analyses Results', 'A', '')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Selected KPIs', 'K', '', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Raw Data', 'D', '')
        self.Params.Output.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'KPI Names', 'KN', '')
        self.Params.Output.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'KPI Values', 'KV', '')
        self.Params.Output.Add(p)

    def SolveInstance(self, DA):
        A = self.marshal.GetInput(DA, 0)
        K = self.marshal.GetInput(DA, 1)
        results = self.RunScript(A, K)

        if results is not None:
            self.marshal.SetOutput(results[0], DA, 0, True)
            self.marshal.SetOutput(results[1], DA, 1, True)
            self.marshal.SetOutput(results[2], DA, 2, True)

    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACqElEQVRIie1SXUhTYRh+3jltKrhjE3WUejTJLARTSrJS+wNFgxnVTbs4QrsIghl04YWQ4lVXRRAIu/BEF4IXNW+KyGAEkRexZpquXKXr91S2LZ3b8ueLM3fWws2WRFc+8MJ3vvc9z/M+7/diAxtIGiSYuL+qXwd5u7bBaV505k/5P3FtTLQ4/snzyOQkmLpIPOmhL3pWwsCO+9PYgYG9HhJM7X/8P5nO0ZBjZs0+DjljIIygGD5UAeHw23n0igdt32Y1squpeDwpCTuvrO5A/b5+dn5/I2pJg4zPACQQJGQjBD0Qjhq9Fxfrnbz0PksY1x99AYfduaaTaOdFdWbW3MShKgNIGQMwEolnYSeF8wFk3q+ICsmR+T3cA3pfllulhXTZlVfhVf9GvlBnZnwTh106IEvOBOKOcd6tgyH9CkoLiqJ3dx89hNszil5uwHBd2tkwJJhamWixyTlVpKYdM7WXmN/IQaVb65kSIn2TBmU79qBTdRpnAi4ueyl0mwQTH3XCREsXCbAR+9jHlow8UJAUsevtdPS8NS8P7yQJRw63oE3yWAFER6Y4QdgavdlNH3quwv1gXW5koZgRGpSDOrYoonyBBAwi9LyPnT3BIzcx6aR7etXdltw8aNLSDCoi3g+ISLTCcNinqLDgBj2x50O1VIltWFmdyApnfE1Fhe8UdFouHPJ7TLxyobykFLeG7iEQDHr9weAhOOzexCIrQkEMPx4kdf4ITfoaUbaoAWZAd3IQGKi21Wyv5pXSVLUaE69dGB59iuXlZXjm5i4z0WJV8qqEIr9GaMX4XDH1/LBSR4kN1vJiNqvplnPy/JUwNhtAlIJjtXWrONTxiOMIybZblW9lNWOxWatFp+kcAsGQfOuNzSUlEgeOa/03u9fIi+vk3cD/AICf2w7z7OScV4oAAAAASUVORK5CYII='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, A, K):
        results = None
        
        # Set icons and message 
        cur_analysis = A['type']
        self.SetIconOverride(utils.get_icon(cur_analysis))
        self.Message = cur_analysis
        
        defaultFilter = 'current'
        all_kpi = list(A['kpi']['current'].keys())
        bitmap = System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(
                    A['b64']
                )
            )
        )
        
        if not K:
            K = all_kpi
        KPI_names = K
        
        defaultFilter = 'current'
        KPI_values = tree[object]()
        KPI_names = []
        KPI_args = tree[object]()
        
        pthCnt = 0
        KPI = A['kpi']
        for n, nme in enumerate(K):
            #extract kpi
            for name, val in KPI[defaultFilter].items():
                if name == nme:
                    if type(val['val']) == list:
                        #unpack list (the case when kpi was a histogram)
                        for l in val['val']:
                            pth = ghpath(pthCnt)
                            #add args
                            try:
                                KPI_values.AddRange(l, pth)
                                KPI_names.append(name)
                                pthCnt += 1
                            except:
                                KPI_values.Add(l,pth)
                                KPI_names.append(name)
                                pthCnt += 1
                                
                    else:
                        pth = ghpath(pthCnt)
                        KPI_names.append(name)
                        KPI_values.Add(val['val'], pth)
                        #add args
                        pthCnt+=1
        
        raw_data = bitmap
        KPI = [A['kpi']]
        HTML = A['metrics']

        results = (raw_data, KPI_names, KPI_values)
        
        return results

class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):
    def get_AssemblyName(self):
        return 'ReadAnalyses'
    
    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return 'City Intelligence Lab'

    def get_AuthorName(self):
        return ''
    
    def get_Id(self):
        return System.Guid('869e4089-d2e5-4472-85b5-a7ba9d58a1b9')