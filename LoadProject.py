# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import scriptcontext as sc
import System
import Rhino
import json

import InFraReDUtils as utils

class LoadProject(component):

    def __init__(self):
        super(LoadProject, self).__init__()

        # Defaults (None indicates required input params)
        self.N = None

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Load Project',
            'IR Load Proj',
            'Use this component once at the start of your workflow to load a project from the InFraReD server by name.',
            'InFraReD',
            '2 | Project'
        )
        return instance
    
    def get_ComponentGuid(self):
        return System.Guid('3e03c698-1791-4944-8e37-a4701405610e')
    
    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional
    
    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Project Name', 'N', 'The name of the project you would like to load.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_Rectangle()
        self.SetUpParam(p, 'Site Boundary', 'Sb', 'A rect that represents the bounds of your project area.')
        self.Params.Output.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Project Metadata', 'm', 'Metadata of the loaded project in the InFraReD Schema format.')
        self.Params.Output.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Snapshots', 's', 'The names of all the snapshots in the project.')
        self.Params.Output.Add(p)
    
    def SolveInstance(self, DA):
        N = self.marshal.GetInput(DA, 0)
        results = self.RunScript(N)

        if results is not None:
            self.marshal.SetOutput(results[0], DA, 0, True)
            self.marshal.SetOutput(results[1], DA, 1, True)
            self.marshal.SetOutput(results[2], DA, 2, True)
        
    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABw0lEQVRIib3WzSsEYRwH8O+s0WawyxKbYrdVLAeGsomEO1niIMkiBwelHJxEufgPyJLdEjdZL7kiOUiazYWD8nLCKmG2UOh52t1i51XG9zK1v9/Mp5nn98wOAwCMb8gLgMffJ/AZ8F8yjG9oCsCkAQDJI4AqE4BGgwCSLABek4FAAtKNOHNzdCu6kKVBH3bGRsEXFRqDEOBOjGJ8LQT/QJ8uSBMSB0jyLZlYOTrWBakiccCRY0NJfh4yzGZ08BW6IEVk0tuaAGY2t3EVidDfJ9ZCcNmyE5DaMCgidqsV5QV2CgjXN99qI8urFHoQRWRxnCLCKhWHg8v0Ao/RqGSdQEr1eFTXRO0CanXo3SfpZrOe9t8hW0IYHkdR0vr8Cmlyl9LJ+rmgoRMB3bPzSY+orZqX7FdEeupq8fT2rnmztfCV+GBMsv2SiPj6So9WLk0TQvpfYudIRXKEUxgGt0/P6PLUoLnMrYqwbCrsFovspEkii/sHmO5sx0b4FBf3EUWAvGbqi13g2BTZgZBESHPv3AJd0AaXU/VOdoQwds/OZeuyO57cevDgUBXQkv/4+6UI+aIwMuvkcfXHBPJl8dfZ+wz4hS8suJU2F0HXlAAAAABJRU5ErkJggg=='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, N):
        results = None

        # Validate authentication
        auth_token = sc.sticky.get('InFraReDAuthToken')
        if not auth_token:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                'Invalid Credentials.\nPlease authenticate with the InFraReD server using the "InFraReD Authenticate" component.'
            )
            return
        
        # Grab stickies
        client_uuid = sc.sticky.get('InFraReDClientUuid')

        # Input parameter checking and defaults
        if N is not None:
            self.N = N
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter N failed to collect data.'
            )
            return
        
        # GQL request to get projects
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getProjectsByUserUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            client_uuid
        )

        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)

        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['getProjectsByUserUuid']
        if return_data['success'] == True:
            projects = return_data['infraredSchema']['clients'][client_uuid]['projects']
        
        # Check for projects
        if len(projects) == 0:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                'No projects found.\nPlease use the "InFraReD Create Project" component to create a new project instead.'
            )
            return
        
        # Get project by project name
        for u, v in projects.items():
            if v['projectName'] == self.N:
                project_uuid = u
                project = v
                break
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                'Invalid project name (N).\nNo projects found with that exact name. Please check that the Name (N) can be found in the Projects (p) output from the "InFraReD Authenticate" component.'
            )
            return
        
        # Rebuild site boundary
        o_x, o_y = project['projectSiteBoundary']['coordinates'][0][0]
        w, h = project['projectSiteBoundary']['coordinates'][0][2]
        Sb = Rhino.Geometry.Rectangle3d(
            Rhino.Geometry.Plane(
                Rhino.Geometry.Point3d(o_x, o_y, 0),
                Rhino.Geometry.Point3d(o_x + 1, o_y, 0),
                Rhino.Geometry.Point3d(o_x, o_y + 1, 0)
            ),
            w - o_x,
            h - o_y
        )
        
        # GQL request to get server-generated origin snapshot from project creation
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getSnapshotsByProjectUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            project_uuid
        )

        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['getSnapshotsByProjectUuid']
        if return_data['success'] == True:
            snapshots = return_data['infraredSchema']['clients'][client_uuid]['projects'][project_uuid]['snapshots']
            s = [x['snapshotName'] for x in snapshots.values()]
        
        # Store stickies
        sc.sticky['InFraReDAuthToken'] = auth_token
        sc.sticky['InFraReDProjectUuid'] = project_uuid
        sc.sticky['InFraReDProjectList'] = projects
        sc.sticky['InFraReDSnapshotList'] = snapshots
        
        m = json.dumps(project, ensure_ascii=False)
        return Sb, m, s

class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'LoadProject'
    
    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'
    
    def get_Id(self):
        return System.Guid('3a048113-2e89-41ce-af23-1fdf4ed58670')
