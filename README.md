# InFraReD Plugin for Grasshopper

A Grasshopper plugin for the Intelligent Framework for Resilient Design (InFraReD).

The plugin was first demo'd in a closed alpha test as part of the 2021 DigitalFUTURES workshops. Watch the demo [here](https://youtu.be/60TxGma7l1w?t=8192)!

For more information, please visit [infrared.city](infrared.city).
