import clr

# 0 | Authenticate
clr.CompileModules('InFraReD/Authenticate.p2.7.8.0.ghpy', 'Authenticate.py', 'InFraReDUtils.py')

# 1 | Analysis
clr.CompileModules('InFraReD/WindSpeedAnalysis.p2.7.8.0.ghpy', 'WindSpeedAnalysis.py', 'InFraReDUtils.py')
clr.CompileModules('InFraReD/WindComfortAnalysis.p2.7.8.0.ghpy', 'WindComfortAnalysis.py', 'InFraReDUtils.py')
clr.CompileModules('InFraReD/SolarRadiationAnalysis.p2.7.8.0.ghpy', 'SolarRadiationAnalysis.py', 'InFraReDUtils.py')
clr.CompileModules('InFraReD/SunlightHoursAnalysis.p2.7.8.0.ghpy', 'SunlightHoursAnalysis.py', 'InFraReDUtils.py')

# 2 | Project
clr.CompileModules('InFraReD/CreateProject.p2.7.8.0.ghpy', 'CreateProject.py', 'InFraReDUtils.py')
clr.CompileModules('InFraReD/LoadProject.p2.7.8.0.ghpy', 'LoadProject.py', 'InFraReDUtils.py')

# 3 | Geometry
clr.CompileModules('InFraReD/CreateBuildings.p2.7.8.0.ghpy', 'CreateBuildings.py', 'InFraReDUtils.py')
clr.CompileModules('InFraReD/CreatePOIs.p2.7.8.0.ghpy', 'CreatePOIs.py', 'InFraReDUtils.py')
clr.CompileModules('InFraReD/CreateStreets.p2.7.8.0.ghpy', 'CreateStreets.py', 'InFraReDUtils.py')

# 4 | Snapshot
clr.CompileModules('InFraReD/SyncSnapshot.p2.7.8.0.ghpy', 'SyncSnapshot.py', 'InFraReDUtils.py')

# 5 | Run
clr.CompileModules('InFraReD/RunAnalyses.p2.7.8.0.ghpy', 'RunAnalyses.py', 'InFraReDUtils.py')
clr.CompileModules('InFraReD/ReadAnalyses.p2.7.8.0.ghpy', 'ReadAnalyses.py', 'InFraReDUtils.py')


print('success!')