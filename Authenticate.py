# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System
import json
import datetime
import scriptcontext as sc

import InFraReDUtils as utils

class Authenticate(component):

    def __init__(self):
        super(Authenticate, self).__init__()
        
        # Defaults (None indicates required input params)
        self.U = None
        self.P = None

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Authenticate',
            'IR Auth',
            "Use this component once at the start of your workflow to authenticate your InFraReD user credentials with the InFraReD server.\n\nAll other InFraReD Grasshopper components will require this Authenticate component to run successfully.\n\nIf you don't execute an InFraReD component within an hour, your authentication token may expire and you must run this Authenticate component again.",
            'InFraReD',
            '0 | Authenticate'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid('6bbb1cfd-3232-4549-988f-ba76397722fc')

    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Username', 'U', 'Your InFraReD username.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Password', 'P', 'Your InFraReD password.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Projects', 'p', 'The names of all your projects.')
        self.Params.Output.Add(p)

    def SolveInstance(self, DA):
        U = self.marshal.GetInput(DA, 0)
        P = self.marshal.GetInput(DA, 1)
        result = self.RunScript(U, P)
        
        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)

    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACc0lEQVRIibWWXUhTYRjH/2dJy1lrmkKghhAsu4htEV2IZFYEYZYEEXXTQkT6GLarvIkY3dSFDC9jFw3JlglpiWBWLC9MCtkmUWkxWa4g2kZrubUT+4jnxY029/GedH84nHOed2e/87zv/3nOKwCAoO/SA2jA+suatFo8gqDvMgO4WgIAKQhAJwOgLRGApALQUbZe/2Y+ewbaHfUYdTrRP/kiA8QNOblXi+NaDcKiiA2CgL6JSXj8gfR4S6Mah273wWG6ng2BjAdwo6MdTWo1bLMOPHn7Ds8WPmH4ykWoFAo2fr65CQ3V2zBiuASVopy9kGTI7tpaPH3/IX2/LIowP7ej5+hhdk8AAlI2dKZpkwyJxmKrYt9CIVRWVPA8Dq412SzfmCMmRzKRZNdT8x9hwlh67LHDJR3i9QWws6Yabp8/HTu3fx96h4bZ9cv5BXasKROjbYgt/gnNHgTCEchlMtwaG89wVyFxW5hsmVrQYCQC15KX99HikIONu9Dd2gIxkcCc9wuLbVcq0dveht/RKIy2hwz635C7nXqEY3FYpmcy4nP4moaN9FyGcfBBwczyWpjaxCvPZzxyuvL9hNn45vgEyyq7NopCaIoqlVsy3FRI99/M4lrbMWmQzgPNGJh5zQXASgdY+hHMm01OSPkmOTcgpWn34qqelRdCfWhZ/CMZQtnUVVXxQdaiWDzOB6Eq/hUO45SO/4NJVjYeacUd+1TO8Zx1YrhnY/N7WqdBzVYl3N/9zK7Z0tTXoUwQ4PsZgmFgMG+boY2EnVxb6E3JNakP1L+iAixW7QBMlAntKApKSp/KoVGCXFgBlWLf1Z+0Wlx/ASRM1vOtjGjTAAAAAElFTkSuQmCC'
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, U, P):
        result = None
        
        # Clean up stickies
        for k, v in sc.sticky.items():
            if k.startswith('InFraReD'):
                sc.sticky[k] = None
        
        # Clean up message
        self.Message = ''
        
        # Input parameter checking and defaults
        if U is not None:
            self.U = U
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter U failed to collect data.'
            )
            return
        if P is not None:
            self.P = P
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter U failed to collect data.'
            )
            return
        
        # Set up request
        request = System.Net.HttpWebRequest.Create('https://prototype.infrared.city')
        request.Method = 'POST'
        request.ContentType = 'application/x-www-form-urlencoded'
        request.CookieContainer = System.Net.CookieContainer()
        
        # Add creds as form data
        escape = lambda x: System.Uri.EscapeDataString(x)
        form_data = 'username={}&password={}'.format(escape(self.U), escape(self.P))
        byte_data = System.Text.Encoding.UTF8.GetBytes(form_data)
        request.ContentLength = byte_data.Length
        try:
            # Prepare request
            request_stream = request.GetRequestStream()
            request_stream.Write(byte_data, 0, byte_data.Length)
            request_stream.Close()
            
            # Send request
            response = request.GetResponse()
        except:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                "Authentication failed.\nPlease check your credentials and make sure you're connected to the internet."
            )
            return
        
        # Extract cookies
        for cookie in response.Cookies.GetEnumerator():
            if cookie.Name == 'InFraReD':
                auth_token = cookie.Value
            elif cookie.Name == 'InFraReDClientUuid':
                client_uuid = cookie.Value
        
        response.Close()
        
        # GQL to get all projects
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getProjectsByUserUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            client_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Extract project data
        return_data = json.loads(out)['data']['getProjectsByUserUuid']
        if return_data['success'] == True:
            projects = return_data['infraredSchema']['clients'][client_uuid]['projects']
            p = [proj['projectName'] for proj in projects.values()]
            
        # Set message
        now = datetime.datetime.now()
        self.Message = 'Logged in as\n"{}"\n[{}]'.format(self.U, now.strftime('%X'))
        
        # Store stickies
        sc.sticky['InFraReDAuthToken'] = auth_token
        sc.sticky['InFraReDClientUuid'] = client_uuid
        sc.sticky['InFraReDProjectList'] = projects
        
        result = p
        return result


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'Authenticate'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('66ff2351-7c5d-4fcf-b53f-2fe25466c8dd')